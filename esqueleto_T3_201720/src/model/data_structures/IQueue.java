package model.data_structures;

public interface IQueue<E> {
	
	public void enqueue(E item);
	
	public E dequeue();

}
